# fxForwardPoints

FX forward curve is also called FX implied forward curve or FX derived curve. It is derived from USD zero rate curve and FX forward spreads and used to value FX trades. 